const fs = require('fs').promises

function createDirectory() {
    return fs.mkdir('./randomFiles', { recursive: true })
}


function deleteFileFun() {

    let promiseArray = []
    for (let index = 0; index < 5; index++) {

        const status = fs.unlink(`./randomFiles/fileIndex${index}`)
            .then(() => {
                console.log(`fileIndex${index} successfully deleted ! `)
            }).catch((err) => {
                throw new Error(`error occured while deleting file fileIndex${index},${err.message}`)
            })
        promiseArray.push(status)

    }
    return Promise.all(promiseArray)
}


function writeFileFun() {

    let promiseArray = []
    for (let index = 0; index < 5; index++) {

        const status = fs.writeFile(`./randomFiles/fileIndex${index}`, JSON.stringify(`this is a content for fileIndex${index}`), { encoding: "utf8" }).then(() => {
            console.log(`fileIndex${index} created successfully !`)
        }).catch((err) => {
            throw new Error(`error occured while creating file fileIndex${index},${err.message}`)
        })
        promiseArray.push(status)

    }
    return Promise.all(promiseArray)
}

module.exports = { createDirectory, writeFileFun, deleteFileFun }
const { createDirectory, writeFileFun, deleteFileFun } = require('./../problem1')

createDirectory().then(() => {
    return writeFileFun()
}).then(() => {
    return deleteFileFun()
}).catch((err) => {
    console.error(err.message)
})

/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/


const { readFileFun, writeFileFun, appendFileFun, deleteFileFun } = require("./../problem2")

readFileFun('./data/lipsum.txt')
    .then((data) => {

        console.log(`file successfully read !`)
        const upperCaseData = (data).toUpperCase()
        const fileName = './upperCaseDataFile.txt';

        writeFileFun(fileName, upperCaseData)
        console.log(`file successfully written ${fileName}`)
        return fileName

    }).then((fileName) => {

        appendFileFun(fileName)
        console.log(`file Name ${fileName} successfully written in filenames.txt `)
        return fileName

    }).then((fileName) => {

        return readFileFun(fileName)

    }).then((data) => {

        console.log(`file successfully read !`)
        const lowerCaseData = JSON.parse(data).toLowerCase()
        const splittedData = lowerCaseData.split(".")
        return splittedData

    }).then((splittedData) => {

        const fileNameSplitData = './splitData.txt'
        writeFileFun(fileNameSplitData, splittedData)
        console.log(`file successfully written ${fileNameSplitData}`)
        return fileNameSplitData

    }).then((fileNameSplitData) => {

        appendFileFun(fileNameSplitData)
        console.log(`file Name ${fileNameSplitData} successfully written in filenames.txt `)
        return fileNameSplitData

    }).then((fileNameSplitData) => {

        return readFileFun(fileNameSplitData)

    }).then((data) => {

        console.log("file successfully read ")
        const sortedData = JSON.parse(data).sort()
        const fileNameSortedData = './sortedData.txt'

        writeFileFun(fileNameSortedData, sortedData)
        console.log(`file successfully written ${fileNameSortedData}`)
        return fileNameSortedData

    }).then((fileNameSortedData) => {

        appendFileFun(fileNameSortedData)
        console.log(`file Name ${fileNameSortedData} successfully written in filenames.txt `)

    }).then(() => {

        const filePath = './filenames.txt'
        return readFileFun(filePath)

    }).then((data) => {
        console.log("file read successfully !")
        data = data.split('\n')

        const promise = []
        data.forEach((element) => {
            if (element != "") {
                let status = deleteFileFun(element)
                promise.push(status)
            }
        })
        return Promise.all(promise)

    }).then(() => {

        console.log("all the file successfully deleted ! ")

    }).catch((err) => {

        console.log("err message", err.message)

    })